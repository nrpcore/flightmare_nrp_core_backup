//
// NRP Core - Backend infrastructure to synchronize simulations
//
// Copyright 2020-2021 NRP Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This project has received funding from the European Union’s Horizon 2020
// Framework Programme for Research and Innovation under the Specific Grant
// Agreement No. 945539 (Human Brain Project SGA3).
//

#include "nrp_set_joint_velocity/nrp_set_joint_velocity.h"

#include "nrp_communication_controller/nrp_communication_controller.h"
#include "nrp_general_library/utils/nrp_exceptions.h"

#include <algorithm>
#include <exception>

#include <gazebo/physics/Joint.hh>
#include <gazebo/physics/JointController.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/physics/World.hh>


using namespace nlohmann;


gazebo::NRPJointController::PIDConfig::PIDConfig(double p, double i, double d, gazebo::NRPJointController::PIDConfig::PID_TYPE _type)
    : gazebo::common::PID(p, i, d), Type(_type)
{}

gazebo::NRPJointController::PIDConfig::PIDConfig(const PIDConfig& pid)
    : gazebo::common::PID(pid.GetPGain(), pid.GetIGain(), pid.GetDGain(),
                          pid.GetIMax(), pid.GetIMin(), pid.GetCmdMax(), pid.GetCmdMin()),
                          Type(pid.Type)
{}

gazebo::NRPJointController::PIDConfig::PID_TYPE gazebo::NRPJointController::PIDConfig::convertStringToType(std::string type)
{
    std::transform(type.begin(), type.end(), type.begin(), ::tolower);

    if(type.compare("position") == 0)
        return POSITION;

    if(type.compare("velocity") == 0)
        return VELOCITY;

    throw NRPException::logCreate("No PID of type " + type + " known");
}

gazebo::NRPJointController::~NRPJointController() = default;

void gazebo::NRPJointController::Load(gazebo::physics::ModelPtr model, sdf::ElementPtr sdf)
{
    std::map<std::string, double> jointVelocityInitialValueContainer;

    // Iterate over sdf configurations
    try
    {
        // Read written configurations under the plugin in the sdf file
        sdf::ElementPtr sdfReader = sdf->GetFirstElement();
        while(sdfReader != nullptr)
        {   
            //Get written configuration under the plugin line by line
            //Not these are defined by the user in the sdf file.Basically we are reading these written text files
            //There might be typo in these text and therefore it is neccesarry to check and validate them.
            const std::string jointName = sdfReader->GetName();
            const double jointInitialValue = sdfReader->Get<double>("InitialValue");

            std::cout << "Erdi test join names : "  << jointName << std::endl;
            std::cout << "initial-value :" << sdfReader->Get<double>("InitialValue") << std::endl;

            // Find corresponding joint data
            gazebo::physics::JointPtr pJoint = model->GetJoint(jointName);
            if(pJoint == nullptr)
                throw NRPException::logCreate("Joint \"" + jointName + "\" not found in model \"" + model->GetScopedName() + "\"");

            pJoint->SetVelocity(0, 100);
            // Save config for later
            jointVelocityInitialValueContainer.emplace(jointName, jointInitialValue);

            sdfReader = sdfReader->GetNextElement();
        }
    }
    catch(std::exception &e)
    {
        throw NRPException::logCreate(e, "Error reading configuration for plugin \"" + this->GetHandle() + "\" of model \"" + model->GetScopedName() + "\"");
    }

    // Initiate PID controllers with read values
    auto jointControllerPtr = model->GetJointController();

    // Initiate datapack interfaces for joints
    const auto &joints = model->GetJoints();
    for(const auto &joint : joints)
    {
        const auto jointName = joint->GetName();

        std::cout << "joint_scope_name " << jointName << std::endl;

        // // Check for existing PID Config
        // auto pidConfigIterator = jointConfigs.find(joint->GetName());
        // if(pidConfigIterator != jointConfigs.end())
        // {
        //     // Apply configuration
        //     const auto &pidConfig = pidConfigIterator->second;
        //     if(pidConfig.Type == PIDConfig::PID_TYPE::POSITION)
        //         jointControllerPtr->SetPositionPID(jointName, pidConfig);
        //     else if(pidConfig.Type == PIDConfig::PID_TYPE::VELOCITY)
        //         jointControllerPtr->SetVelocityPID(jointName, pidConfig);
        //     else
        //     {   /* TODO: Handle invalid controller type (Should already have been done with PIDConfig::convertStringToType, but maybe make sure here as well?) */}
        // }

        // Create datapack
        const auto datapackName = NRPCommunicationController::createDataPackName(*this, joint->GetName());

        NRPLogger::info("Registering joint velocity for joint [ {} ]", jointName);
        this->_jointDataPackControllers.push_back(JointDataPackController(joint, jointControllerPtr, jointName));
        NRPCommunicationController::getInstance().registerDataPack(datapackName, &(this->_jointDataPackControllers.back()));
    }

    // Register plugin
    NRPCommunicationController::getInstance().registerModelPlugin(this);
}
