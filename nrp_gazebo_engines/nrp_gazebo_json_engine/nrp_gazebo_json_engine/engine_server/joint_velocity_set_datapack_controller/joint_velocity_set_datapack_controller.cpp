//
// NRP Core - Backend infrastructure to synchronize simulations
//
// Copyright 2020-2021 NRP Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This project has received funding from the European Union’s Horizon 2020
// Framework Programme for Research and Innovation under the Specific Grant
// Agreement No. 945539 (Human Brain Project SGA3).
//

#include "nrp_gazebo_json_engine/engine_server/joint_velocity_set_datapack_controller/joint_velocity_set_datapack_controller.h"

JointDataPackController::JointDataPackController(const gazebo::physics::JointPtr &joint, const gazebo::physics::JointControllerPtr &jointController, const std::string &jointName)
                : JsonDataPackController(JsonDataPack::createID(jointName, "")),
                  _joint(joint),
                  _jointController(jointController)
            {
                std::cout << "Constructor " << jointName << std::endl;
            }

        void JointDataPackController::handleDataPackData(const nlohmann::json &data)
            {
                setCachedData(data);

                

                // const auto &jointName = this->_datapackId.Name;
                const auto cachedData = getCachedData();

                // std::cout << jointName << *cachedData << std::endl;

                if(!(*cachedData)["velocity"].is_null())
                    this->_joint->SetVelocity(0, (*cachedData)["velocity"].get<float>());

            }

        nlohmann::json * JointDataPackController::getDataPackInformation()
            {
                auto data = getCachedData();

                std::cout << "jointName" << *data << std::endl;

                // (*data)["position"] = this->_joint->Position(0);
                (*data)["velocity"] = this->_joint->GetVelocity(0);
                // (*data)["effort"]   = this->_joint->GetForce(0);

                return &(this->_data);
            }            