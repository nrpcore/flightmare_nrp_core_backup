from nrp_core import *
from nrp_core.event_loop import *
import nrp_core.data.nrp_ros.geometry_msgs

@RosPublisher(keyword="flightmare_msg", address="/hummingbird/autopilot/pose_command", type=nrp_core.data.nrp_ros.geometry_msgs.PoseStamped)
@RosPublisher(keyword="msg", address="/husky/cmd_vel", type=nrp_core.data.nrp_ros.geometry_msgs.Twist)
@FromEngine(keyword='actors', address='/nest/actors')
@FunctionalNode(name="gazebo", outputs=['flightmare_msg','msg'])
def transceiver_function(actors):
    left_voltage = actors.data[0]['V_m']
    right_voltage = actors.data[1]['V_m']

    flightmare_msg = nrp_core.data.nrp_ros.geometry_msgs.PoseStamped()
    flightmare_msg.header.seq = 1
    flightmare_msg.pose.position.x = 50 * left_voltage + 50 * right_voltage
    flightmare_msg.pose.position.y = 50 * left_voltage + 50 * right_voltage
    flightmare_msg.pose.position.z = 2 + 50 * left_voltage + 50 * right_voltage
    flightmare_msg.pose.orientation.x = 0
    flightmare_msg.pose.orientation.y = 0
    flightmare_msg.pose.orientation.z = 0


    
    print("left_voltage: ", left_voltage)
    print("right_voltage: ", right_voltage)
    msg = nrp_core.data.nrp_ros.geometry_msgs.Twist()
    msg.linear.x=0
    msg.linear.y=0.0
    msg.linear.z=0.0
    msg.angular.x=0.0
    msg.angular.y=0.0
    msg.angular.z=6

    return [flightmare_msg,msg]

